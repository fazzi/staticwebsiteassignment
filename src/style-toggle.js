document.addEventListener("DOMContentLoaded", function () {
  const styleToggleButton = document.getElementById("style-toggle");
  const currentStyle = localStorage.getItem("style");

  if (currentStyle === "alt") {
    document
      .getElementById("theme-style")
      .setAttribute("href", "src/alt-style.css");
  } else {
    document
      .getElementById("theme-style")
      .setAttribute("href", "src/original-style.css");
  }

  styleToggleButton.addEventListener("click", function () {
    const currentHref = document
      .getElementById("theme-style")
      .getAttribute("href");
    if (currentHref.includes("alt-style.css")) {
      document
        .getElementById("theme-style")
        .setAttribute("href", "src/original-style.css");
      localStorage.setItem("style", "original");
    } else {
      document
        .getElementById("theme-style")
        .setAttribute("href", "src/alt-style.css");
      localStorage.setItem("style", "alt");
    }
  });
});
