setInterval(function () {
  var elements = document.querySelectorAll('[id^="image"]');
  elements.forEach(function (element) {
    element.classList.add("rotate");
  });
  setTimeout(function () {
    elements.forEach(function (element) {
      element.classList.remove("rotate");
    });
  }, 200);
}, 1000); // 10 seconds in milliseconds
